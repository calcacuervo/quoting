package com.britecore.rules.quoting;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Item implements Serializable{

	private String name;

	private Map<String, Object> data;

	public Item() {
	}

	public Item(String fieldName, Map<String, Object> elements) {
		this.name = fieldName;
		this.data = elements;
	}

	public Item(String fieldName) {
		this(fieldName, new HashMap<>());
	}

	public Map<String, Object> getData() {
		return data;
	}

	public String getName() {
		return name;
	}

	public void setData(Map<String, Object> elements) {
		this.data = elements;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void addData(String name, Serializable data) {
		this.data.put(name, data);
	}

	@Override
	public String toString() {
		return "Item [name=" + name + ", data=" + data + "]";
	}

}
