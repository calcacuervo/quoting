package com.britecore.rules.quoting.helper;

import java.util.Map;
import java.util.Optional;

public class MapHelper {
    public static String string(Map<String, Object> map, String key) {
        return Optional.ofNullable(map.get(key)).map(o->o.toString()).orElse(null);
    }
}
