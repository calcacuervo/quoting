package com.britecore.rules.quoting.helper;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class DateUtils {

	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	public static Long getYears(String fromDate) {
		LocalDate localDate = LocalDate.parse(fromDate, formatter);
		return ChronoUnit.YEARS.between(localDate, LocalDate.now());
	}

	public static Long getYears(Date fromDate) {
		return ChronoUnit.YEARS.between(LocalDate.ofEpochDay(fromDate.getTime()), LocalDate.now());
	}
	
	public static Long getYears(int year) {
		return Math.abs(ChronoUnit.YEARS.between(LocalDate.of(year, 1, 1), LocalDate.now()));
	}
}
