package com.britecore.rules.quoting.helper;

import com.britecore.rules.common.model.ContextData;
import com.britecore.rules.quoting.Field;
import com.britecore.rules.quoting.Item;
import com.britecore.rules.quoting.Line;
import com.britecore.rules.quoting.QuoteData;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;
import static com.britecore.rules.quoting.helper.MapHelper.*;

public class ContextDataHelper {

    public static QuoteData getQuoteData(final ContextData context) {
        Map<String, Object> contextForm = context.getContext().get("quote");
        if (contextForm == null) {
            return new QuoteData();
        }
        Map<String, Map<String, Object>> fields =  (Map<String, Map<String, Object>>) contextForm.get("items");
        if (fields == null) {
            return new QuoteData(new HashMap<>());
        }
        return new QuoteData(fields);
    }

    public static List<Field> getFields(final ContextData context) {
        Map<String, Object> contextForm = context.getContext().get("form");
        if (contextForm == null) {
            return null;
        }
        List<Map<String, Serializable>> fields = (List<Map<String, Serializable>>) contextForm.get("fields");
        return fields.stream().map(o -> new Field(o.get("name").toString(), o)).collect(Collectors.toList());
    }

    public static List<Item> getItems(final ContextData context) {
        return getQuoteData(context).getItems().stream().map(qi-> new Item(qi.getName(), qi.getData())).collect(Collectors.toList());
    }

    public static Line getLine(final ContextData contextData) {
        Map<String, Object> contextForm = contextData.getContext().get("line");
        return new Line(string(contextForm, "code"), string(contextForm,"name"));
    }
}
