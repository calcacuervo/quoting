package com.britecore.rules.quoting;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class QuoteData implements Serializable {

	private List<QuoteItem> items;

	public QuoteData(Map<String, Map<String, Object>> originalItems) {
		items = originalItems.entrySet().stream().map(i -> new QuoteItem(i.getKey(), i.getValue()))
				.collect(Collectors.toList());
	}

	public QuoteData() {
		this(new HashMap<>());
	}

	public void setItems(List<QuoteItem> items) {
		this.items = items;
	}

	public List<QuoteItem> getItems() {
		return items;
	}

	public static class QuoteItem {
		private String name;
		private Map<String, Object> data;

		public QuoteItem(String name, Map<String, Object> data) {
			this.name = name;
			this.data = data;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public Map<String, Object> getData() {
			return data;
		}

		public void setData(Map<String, Object> data) {
			this.data = data;
		}

		@Override
		public String toString() {
			return "QuoteItem [name=" + name + ", data=" + data + "]";
		}

	}
}
