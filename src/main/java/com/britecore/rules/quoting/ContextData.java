package com.britecore.rules.quoting;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class ContextData  implements Serializable {
	private List<com.britecore.rules.quoting.ActionData> actions;
	private Map<String, Map<String, Object>> context;

	public List<com.britecore.rules.quoting.ActionData> getActions() {
		return actions;
	}

	public Map<String, Map<String, Object>> getContext() {
		return context;
	}

	public void setActions(List<ActionData> actions) {
		this.actions = actions;
	}

	public void setContext(Map<String, Map<String, Object>> context) {
		this.context = context;
	}
}
