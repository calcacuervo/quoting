package com.britecore.rules.quoting;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Field implements Serializable{

	private String name;

	private Map<String, Serializable> data;

	public Field() {
	}

	public Field(String fieldName, Map<String, Serializable> elements) {
		this.name = fieldName;
		this.data = elements;
	}

	public Field(String fieldName) {
		this(fieldName, new HashMap<>());
	}

	public Map<String, Serializable> getData() {
		return data;
	}

	public String getName() {
		return name;
	}

	public void setData(Map<String, Serializable> elements) {
		this.data = elements;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void addData(String name, Serializable data) {
		this.data.put(name, data);
	}

	@Override
	public String toString() {
		return "Field [name=" + name + ", data=" + data + "]";
	}

}
