package com.britecore.rules.quoting;

import java.io.Serializable;

public class Limits implements Serializable {
	private Integer min;
	private Integer max;

	public Limits() {
	}

	public Limits(Integer min, Integer max) {
		this.min = min;
		this.max = max;
	}

	public Integer getMax() {
		return max;
	}

	public Integer getMin() {
		return min;
	}

	public void setMax(Integer max) {
		this.max = max;
	}

	public void setMin(Integer min) {
		this.min = min;
	}
}
