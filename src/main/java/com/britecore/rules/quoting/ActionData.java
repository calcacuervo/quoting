package com.britecore.rules.quoting;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class ActionData implements Serializable {

	private String name;
	private Map<String, Object> data;
	
	public ActionData() {
	}

	public ActionData(final String name) {
		this(name, new HashMap<>());
	}

	public ActionData(final String name, Map<String, Object> data) {
		this.name = name;
		this.data = data;
	}
	
	public Map<String, Object> getData() {
		return data;
	}
	
	public String getName() {
		return name;
	}
	public void setData(Map<String, Object> data) {
		this.data = data;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public ActionData addParameter(String key, Object o) {
		this.getData().put(key, o);
		return this;
	}
	
}
