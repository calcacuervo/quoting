package com.britecore.rules.quoting;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Action implements Serializable {

	private String type;
	
	private Map<String, Object> data;

	public Action(String type) {
		this.type = type;
		this.data = new HashMap<>();
	}

	public Map<String, Object> getData() {
		return data;
	}
	
	public String getType() {
		return type;
	}
	
	public void setData(Map<String, Object> data) {
		this.data = data;
	}
	
	public void setType(String type) {
		this.type = type;
	}
}
