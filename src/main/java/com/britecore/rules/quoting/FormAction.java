package com.britecore.rules.quoting;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class FormAction implements Serializable {

	private String name;
	
	private Map<String, Object> parameters;
	
	public FormAction(final String name) {
		this.name = name;
		this.parameters = new HashMap<>();
	}
	
	public FormAction() {
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Map<String, Object> getParameters() {
		return parameters;
	}
	
	public void setParameters(Map<String, Object> parameters) {
		this.parameters = parameters;
	}
	
	public FormAction addParameter(String key, Object value) {
		this.parameters.put(key, value);
		return this;
	}

	@Override
	public String toString() {
		return "FormAction{" +
				"name='" + name + '\'' +
				", parameters=" + parameters +
				'}';
	}
}
